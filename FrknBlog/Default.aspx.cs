﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FrknBlog
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["baglan"].ToString());

            
            SqlCommand ustKategoriler = new SqlCommand("Select * from Kategoriler",baglan);
            SqlDataReader kategoriGetir;
            SqlCommand yazilar = new SqlCommand("Select * from vwYazilar", baglan);
            SqlDataReader yaziGetir;


            baglan.Open();
            kategoriGetir = ustKategoriler.ExecuteReader();
            rptUstKategoriler.DataSource = kategoriGetir;
            rptUstKategoriler.DataBind();
            baglan.Close();

            baglan.Open();
            yaziGetir = yazilar.ExecuteReader();
            rptYaziGetir.DataSource = yaziGetir;
            rptYaziGetir.DataBind();

            
        }

      
    }
}