﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="FrknBlog.Default" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title>Frkn Blog Teması</title>
	<link rel="stylesheet" type="text/css" href="stil/sifirlama.css" />
	<link rel="stylesheet" type="text/css" href="stil/stil.css" />
	<link rel="stylesheet" type="text/css" href="stil/animate-custom.css" />
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,800' rel='stylesheet' type='text/css'>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="js/tab.js"></script>
</head>
<body>
<form id="form1" runat="server">
<div id="sabitle">
	<!-- Üst Menü -->
	<div id="ustmenu">
		<div class="ortala">
		
			<!-- Ben Hakkı -->
			<div class="ben animated flipInX">
				<a href="/index.php"><img src="images/ben2.jpg" alt="" /></a>
				<h1>Furkan Topaloğlu</h1>
				<h2>Hoşgeldiniz</h2>
			</div>
			<!-- #Ben Hakkı -->
			
			<!-- Üst Kategori -->
			<div class="ustkat animated flipInY">
				<ul>
					<li><a href="/index.php">Ana Sayfa</a></li>
					<li><a href="/hakki-kimdir.html">Hakkımda</a></li>
					<li><a href="#">Arşiv</a></li>
					<li><a href="/iletisim.html">İletişim</a></li>
				</ul>
				</br>
			<p>"Zamanınız Kısıtlı! Bu yüzden başka insanların gürültüsünün, kendi kalbinizin sesini duymanızı engellemesine izin vermeyin." /Steve Jobs</p>
			</div>
			<!-- #Üst Kategori -->
		</div>
	</div>
	<!-- #Üst Menü -->
</div>
	<div style="clear: both"></div>
	
	<!-- Kategoriler -->
	<div class="kategoriler bounceInDown animated">
		<div class="ortala">
            <asp:Repeater ID="rptUstKategoriler" runat="server">
                <HeaderTemplate><ul></HeaderTemplate>
                <ItemTemplate><li><a href="KategoriGoster.Aspx?id=<%# Eval("id") %>"><%# Eval("baslik") %></a></li></ItemTemplate>
                <FooterTemplate></ul></FooterTemplate>
            </asp:Repeater>
		</div>
	</div>
	<!-- #Kategoriler -->
	<!-- Genel -->
	<div class="ortala">
	
		<!-- Sağ Kısım -->
		<div id="sag" class="animated fadeInRight">
			<!-- Popüler / Son Yorumlar -->
			<div class="tab">
				<a href="#">Popüler Konular</a> /
				<a href="#">Son Yorumlar</a>
			</div>
			<div class="tabContent">
				<!-- Popüler Konu -->
				<div class="popson">
					<div class="popresim"><a href="#" title="Div Class Atama"><img src="images/resim2.jpg" alt="" /></a></div>
					<div class="popresim popkat turuncu"><a href="#">Web Tasarım</a></div>
					<h3><a href="#" title="Div Class Atama">Div Class Atama</a></h3>
					<span>11 Ocak 2014</span>
				</div>
				<!-- #Popüler Konu -->
				<!-- Popüler Konu -->
				<div class="popson">
					<div class="popresim"><a href="#" title="Div Class Atama"><img src="images/resim2.jpg" alt="" /></a></div>
					<div class="popresim popkat turuncu"><a href="#">Web Tasarım</a></div>
					<h3><a href="#" title="Div Class Atama">Div Class Atama</a></h3>
					<span>11 Ocak 2014</span>
				</div>
				<!-- #Popüler Konu -->
			</div>
			<div class="tabContent">
				<div style="padding: 10px">Buraya son yorumlar gelecek.</div>
			</div>
			<!-- #Popüler / Son Yorumlar -->
			
			<!-- Arama -->
			<div class="arama">
				<div class="sagBaslik"><h2>Arama</h2></div>
				<form action="" name="searchform" method="post">
					<input type="text" name="s" value="Birşey mi aramıştınız?" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
					<input type="submit" name="submit" class="aramabuton" value="" />
				</form>
			</div>
			<!-- #Arama -->
			
			<!-- Reklamlar -->
			<div class="reklam">
				<div class="sagBaslik"><h2>Reklamlar</h2></div>
				<a href="#"><img src="images/reklam.png" class="sol" alt="" /></a>
				<a href="#"><img src="images/reklam90.png" class="sol1" alt="" /></a>
				<a href="#"><img src="images/reklam90.png" class="sol2" alt="" /></a>
			</div>
			<!-- #Reklamlar -->
			
			
			<!-- Sosyal Ağlarda Ben -->
			<div class="sosyalaglar">
				<div class="sagBaslik"><h2>Sosyal Ağlar</h2></div>
					<a href="http://www.facebook.com/hgtucelcom" target="_blank"><img src="images/facebook.png" alt="" /></a>
					<a href="http://www.twitter.com/hgtucel" target="_blank"><img src="images/twitter.png" alt="" /></a>
					<a href="https://plus.google.com/u/0/116998164609492583190/posts" target="_blank"><img src="images/gplus.png" alt="" /></a>
					<a href="http://instagram.com/hgtucel" target="_blank"><img src="images/instagram.png" alt="" /></a>
					<a href="http://www.quup.com/hgtucel" target="_blank"><img src="images/quup.png" alt="" /></a>
					<a href="#"><img src="images/youtube.png" alt="" /></a>
			</div>
			<!-- #Sosyal Ağlarda Ben -->
			
			<!-- En Son Ne Dinledim -->
			<div class="ensonnedinledim">
				<div class="sagBaslik"><h2>En Son Ne Dinledim?</h2></div>
					<a href="#" title="Ezel-Cello"><img src="images/ezel.jpg" alt="Ezel-Cello" /></a>
			</div>
			<!-- #En Son Ne Dinledim -->
			
			<!-- Kenan İmirzalıoğlu -->
				<a href="http://www.kimirzalioglu.com" target="_blank" title="Kenan İmirzalıoğlu"><img src="images/kimirzalioglucom.png" alt="" style=" margin-top: 10px" /></a>
			<!-- #Kenan İmirzalıoğlu -->
			
		</div>
		<!-- #Sağ Kısım -->
		
		<!-- Sol Kısım -->
		<div id="sol" class="animated fadeInLeft">


            <asp:Repeater ID="rptYaziGetir" runat="server">
                   <ItemTemplate>
                        <!-- Kısa Konu -->
                       <div class="kKonu">
				            <div class="kKonuResim"><img src="images/resim2.jpg" alt="" /></div>
				            <div class="kKonuResim kKonukat kirmizimsi"><a href="KategoriGoster.Aspx?id=<%# Eval("katid") %>"><%# Eval ("kategori") %></a></div>
				            <div class="kKonuİcerik">
					            <div class="kKonuBaslik">
						            <h2><a href="YaziGoster.Aspx?id=<%# Eval("id") %>" title="<%# Eval("baslik") %>"><%# Eval("baslik") %></a></h2>
					            </div>
					            <div class="kKonuBilgi">
					            11 Ocak 2014 | 914 Görüntülenme
					            </div>
					            <div class="kKonuYazi">
					            <%# Eval("icerik") %> 
					            </div>
				            </div>
			            </div>
                       <!-- #Kısa Konu -->
                   </ItemTemplate>

            </asp:Repeater>
		
			
		
		
			
		</div>
		<!-- #Sol Kısım -->
	</div>
	<!-- #Genel -->
<div style="clear: both"></div>
<!-- Footer -->
<div id="footer">
	<div class="hgtucel">Tasarım: <a href="http://www.furkantopaloglu.com" title="Furkan Topaloğlu">frkntplglu</a></div>
	<div class="footer">
	Tüm hakları saklıdır. 2011-2013 © Furkan Topaloğlu
	</div>
</div>
<!-- #Footer -->
</form>
</body>
</html>

